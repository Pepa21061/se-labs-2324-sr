from query_handler_base import QueryHandlerBase
import random
import requests
import json

class WeatherHandler(QueryHandlerBase):
    def can_process_query(self, query):
        if "weather" in query:
            return True
        return False

    def process(self, query):
        params = query.split()
        lat = params[1]
        lon = params[2]
        try:
            result = self.call_api(lat, lon)

            text = result["city_name"]
            self.ui.say(f"{data}")
            self.ui.say(f"{text}")
        except: 
            self.ui.say("Oh no! There was an error trying to contact weather api.")
            self.ui.say("Try something else!")



    def call_api(self, lat, lon):
        url = "https://weatherbit-v1-mashape.p.rapidapi.com/forecast/3hourly"

        querystring = {"lat":lat,"lon":lon}

        headers = {
            "X-RapidAPI-Key": "892ed3485emsh41ffa946bd1d807p18756cjsn903837c546f9",
            "X-RapidAPI-Host": "weatherbit-v1-mashape.p.rapidapi.com"
        }

        response = requests.request("GET", url, headers=headers, params=querystring)

        return json.loads(response.text)
