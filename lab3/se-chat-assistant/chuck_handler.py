from query_handler_base import QueryHandlerBase
import random
import requests
import json

class ChuckNorrisHandler(QueryHandlerBase):
    def can_process_query(self, query):
        if "chuck" in query:
            return True
        return False

    def process(self, query):

        try:
            result = self.call_api()
            text = result["value"]
            self.ui.say(f"{text}")
        except: 
            self.ui.say("Oh no! There was an error trying to contact Love api.")



    def call_api(self):
        url = "https://matchilling-chuck-norris-jokes-v1.p.rapidapi.com/jokes/random"

        querystring = {}

        headers = {
                "X-RapidAPI-Key": "892ed3485emsh41ffa946bd1d807p18756cjsn903837c546f9",
                "X-RapidAPI-Host": "matchilling-chuck-norris-jokes-v1.p.rapidapi.com"
        }

        response = requests.request("GET", url, headers=headers, params=querystring)

        return json.loads(response.text)
