from query_handler_base import QueryHandlerBase
import random
import requests
import json

class HistoryHandler(QueryHandlerBase):
    def can_process_query(self, query):
        if "history" in query:
            return True
        return False

    def process(self, query):

        try:
            result = self.call_api()

            text = result["article"]["title"]
            date = result["article"]["date"]
            url = result["article"]["url"]
            self.ui.say(f"{text}")
            self.ui.say(f"on date: {date}")
            self.ui.say(f"from page: {url}")
        except: 
            self.ui.say("Try something else!")



    def call_api(self):
        url = "https://today-in-history.p.rapidapi.com/thisday"

        querystring = {}

        headers = {
                "X-RapidAPI-Key": "892ed3485emsh41ffa946bd1d807p18756cjsn903837c546f9",
                "X-RapidAPI-Host": "today-in-history.p.rapidapi.com"
        }

        response = requests.request("GET", url, headers=headers, params=querystring)

        return json.loads(response.text)
