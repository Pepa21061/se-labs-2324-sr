#Input the comma-separated integers and transform them into a list of integers.
#Given a list of ints, return True if the list contains a 2 next to a 2 somewhere.
def onlyNumbers(list):
    listOfStringNumbers = list.split(",")
    listOfIntegers = []

    for i in listOfStringNumbers:
        listOfIntegers.append(int(i))
    return listOfIntegers

def checkAdjecent(list):
    
    
    for i in range(len(list)-1):
        print("trenutna vrijednost= " + str(list[i]))
        if(list[i] == 2 and list[i+1]==2):
            return True
    return False
    


def main():
    userInput1 = "1,2,3,4,100"
    userInput2 = "1,3,5,2,5,2,2"
    userInput3 = "1,7,4,2,2"
    userInput4 = "1,2,2,5,4"

    listOfIntegers1 = onlyNumbers(userInput1)
    listOfIntegers2 = onlyNumbers(userInput2)
    listOfIntegers3 = onlyNumbers(userInput3)
    listOfIntegers4 = onlyNumbers(userInput4)

    print("list1 contains a 2 next to a 2 = " + str(checkAdjecent(listOfIntegers1)))
    print("list2 contains a 2 next to a 2 = " + str(checkAdjecent(listOfIntegers2)))
    print("list3 contains a 2 next to a 2 = " + str(checkAdjecent(listOfIntegers3)))
    print("list4 contains a 2 next to a 2 = " + str(checkAdjecent(listOfIntegers4)))

    
    
if __name__ == "__main__":
    main()
