def onlyNumbers(list):
    listOfStringNumbers = list.split(",")
    listOfIntegers = []

    for i in listOfStringNumbers:
        listOfIntegers.append(int(i))
    return listOfIntegers

def evenNumbers(list):
    counter = 0
    for i in list:
        if i % 2 == 0:
            counter += 1
    return counter


def main():
    userInput1 = "1, 2, 3, 4, 100"
    userInput2 = "2, 2, 0"
    userInput3 = "1, 3, 5"
    
    extractedNumbers1 = onlyNumbers(userInput1)
    n = evenNumbers(extractedNumbers1)
    print("Number of evens for first input-> " + str(n))
    
    extractedNumbers2 = onlyNumbers(userInput2)
    n = evenNumbers(extractedNumbers2)
    print("Number of evens for second input-> " + str(n))
    
    extractedNumbers3 = onlyNumbers(userInput3)
    n = evenNumbers(extractedNumbers3)
    print("Number of evens for third input-> " + str(n))

    
if __name__ == "__main__":
    main()
