import csv
csv_file_path = 'ex2-text.csv'

employees = []
office = []

with open(csv_file_path, 'r') as csv_file:
    csv_reader = csv.reader(csv_file)

    for row in csv_reader:
        employees.append(row[0] + ", " + row[1])
        office.append(row[0] + ", " + row[3])

with open('ex2-employees.txt', "w") as f:
    for item in employees:
        f.write("\n%s" %item)

with open('ex2-locations.txt', "w") as f:
    for item in office:
        f.write("\n%s" %item)
