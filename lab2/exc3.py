import json


csvfile = open("ex2-text.csv", "r")
csvfile.readline()

employees = []
dictEmployees = []

for line in csvfile:
    employees.append(line.split(","))

for employeeData in employees:
    dictEmployees.append(
        {
            'employee': employeeData[0],
            'title': employeeData[1],
            'age': employeeData[2],
            'office': employeeData[3]
        }
    )

#print(dictEmployees)
for row in csvfile:
    print(row)

with open('ex4-employees.json', 'w', encoding='utf-8') as f:
    json.dump(dictEmployees, f)