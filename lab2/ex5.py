import json

class Employee:
    def __init__(self, name, title, age, office):
        self.name = name
        self.title = title
        self.age = age
        self.office = office

    def __str__(self):
        return f"{self.name} ({self.age}), {self.title} @ {self.office}"

with open("ex4-employees.json", "r", encoding="utf-8") as f:
    employees = json.load(f)

#print(employees)
#print("\n\n")

employee_list = []
for e in employees:
    employee_list.append(Employee(e['employee'], e['job title'], e['age'], e['office']))

for e in employee_list:
    print(f'{e.name}, ({e.age}), {e.title}, @ {e.office}')
