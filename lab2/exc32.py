import json
with open("ex2-text.csv", "r" , encoding="utf-8") as f:
    data = f.read()

parseData = data.split("\n")
newData = []
for e in parseData:
    newData.append(e.split(','))

newData.remove(['Employee' , 'job title', 'age','office'])
print(newData)

employees = []
for e in newData:
    employees.append({'employee': e[0], 'job title' : e[1], 'age' : e[2], 'office': e[3]})

print(employees)
with open('ex4-employees.json', 'w', encoding="utf-8") as f:
    json.dump(employees, f)

